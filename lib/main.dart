import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: MyHomePage(title: "Flutter Demo",),
    );
  }
}

class MyFirstWidget extends StatelessWidget {

  String title;  

  MyFirstWidget({this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(title,textAlign: TextAlign.center, style: TextStyle(color: Colors.black, fontSize:15.0)),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _changeColor = false;
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text("My first App"),
        leading: Icon(Icons.apps),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () => print("AppBar"),
            icon: Icon(Icons.settings),
          ),
        ],
      ),
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              width: width,
              height: height,
              color: Colors.greenAccent,
            ),
            Center(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(35.0),
                  color: Colors.white,
                ),      
                width: width / 2,
                height: height / 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(50.0)
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      padding: EdgeInsets.all(5.0),
                      width: width * 0.2,
                      decoration: BoxDecoration(
                        color: Colors.yellow,
                        borderRadius: BorderRadius.circular(20.0)
                      ),
                      child: GestureDetector(
                      child: MyFirstWidget(title: "Change color",),
                      onTap: (){},
                      )
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    MySecondWidget(changeColor: this._changeColor),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            _changeColor = !_changeColor;
          });
        },
        child: Icon(Icons.colorize),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterFloat,
    );
  }
}

class MySecondWidget extends StatefulWidget {
  final bool changeColor;
  
  MySecondWidget({this.changeColor});
  @override
  _MySecondWidgetState createState() => _MySecondWidgetState();
}

class _MySecondWidgetState extends State<MySecondWidget> {
  
  int _count;
  double _height;
  double _width;
  @override
  void initState() { 
    super.initState();
    this._count = 1;
    this._height = 50.0;
    this._width = 50.0;
    print(this.widget.changeColor);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              this._count++;
            });
          },
          child: Container(
            height: this._height,
            width: 50,
            color: Colors.yellow,
            child: Center(
              child: Text("${this._count}",style: TextStyle(fontSize: 20.0)),
            ),
          ),
        ),
        Container(
          width: this._width,
          height: this._height,
          color: (this.widget.changeColor) ? Colors.teal: Colors.deepPurpleAccent,
        ),
        Container(
          width: 50,
          height: 50,
          color: (this.widget.changeColor) ? Colors.deepPurpleAccent: Colors.teal,
        ),
      ],
    );
  }
}